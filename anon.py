#!/usr/bin/env python
import unicodecsv as csv
from faker import Faker
from collections import defaultdict
from pprint import pprint

fake = Faker('en_AU')

fakes = defaultdict(lambda: {
    'User Name': fake.email(),
    'First Name': fake.first_name(),
    'Last Name': fake.last_name()
})

with open('in.csv') as f:
    reader = csv.DictReader(f)
    for row in reader:
        print(row)
        fakerow = fakes[row['User Name']]
        row.update(fakerow)
        print(row)
        print
